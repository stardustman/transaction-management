package com.beerbear.springboottransaction.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//启动时加载此类
@Configuration
//表示此项目启用 Swagger API 文档
@EnableSwagger2
public class Swagger2Config {

    /**
     * api() 方法用于返回实例 Docket（Swagger API 摘要），也是在该方法中指定需要扫描的控制器包路径.
     * 只有此路径下的 Controller 类才会自动生成 Swagger API 文档。
     * 通过 http://localhost:8999/swagger-ui.html 来访问
     * @return
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.beerbear.springboottransaction.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * apiInfo() 方法中主要是配置一些基础信息，包括配置页面展示的基本信息包括，标题、描述、版本、服务条款、联系方式等。
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("swagger-api文档")
                .description("Beer Bear的后台接口")
                .build();
    }
}
