package com.beerbear.springboottransaction.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface GoodsStockMapper {
    void updateStock();
}
