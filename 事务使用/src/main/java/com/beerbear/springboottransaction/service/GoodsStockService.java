package com.beerbear.springboottransaction.service;

import com.beerbear.springboottransaction.util.Result;

public interface GoodsStockService {
    Result firstFunctionAboutException();

    void secondFunctionAboutException();

    void thirdFunctionAboutException() throws Exception;

    void thirdFunctionAboutException1();

    void thirdFunctionAboutException2() throws Exception;

    void privateFunctionCaller();

    void publicFunctionCaller();

    void publicCallee();

    void aopSelfCaller();
}
