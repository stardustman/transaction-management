package com.beerbear.springboottransaction.service.Imp;

import com.beerbear.springboottransaction.dao.GoodsStockMapper;
import com.beerbear.springboottransaction.service.GoodsStockService;
import com.beerbear.springboottransaction.util.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * create by: Beer Bear
 * description: 商品库存服务类
 * create time: 2021/7/25 21:41
 */
@Service
@Slf4j
public class GoodsStockServiceImp implements GoodsStockService {

    @Autowired
    private GoodsStockMapper goodsStockMapper;

    @Override
    @Transactional
    public Result firstFunctionAboutException() {
        try{
            log.info("减库存开始");
            goodsStockMapper.updateStock();
            if(1 == 1) throw new RuntimeException();
            return Result.ok();
        }catch (Exception e){
            log.info("减库存失败！" + e.getMessage());
            return Result.server_error().Message("减库存失败！" + e.getMessage());
        }
    }

    @Override
    @Transactional
    public void secondFunctionAboutException() {
        log.info("减库存开始");
        goodsStockMapper.updateStock();
        if(1 == 1) throw new RuntimeException();
    }

    @Override
    @Transactional
    public void thirdFunctionAboutException() throws Exception {
        log.info("减库存开始");
        goodsStockMapper.updateStock();
        if(1 == 1) throw new Exception();
    }

    @Override
    @Transactional
    public void thirdFunctionAboutException1(){
        try{
            log.info("减库存开始");
            goodsStockMapper.updateStock();
            if(1 == 1) throw new Exception();
        }catch (Exception e){
            log.info("出现异常"+e.getMessage());
            throw new RuntimeException("手动抛出RuntimeException");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void thirdFunctionAboutException2() throws Exception {
        log.info("减库存开始");
        goodsStockMapper.updateStock();
        if(1 == 1) throw new Exception();
    }

    @Override
    public void privateFunctionCaller (){
        privateCallee();
    }

    @Transactional
    private void privateCallee(){
        goodsStockMapper.updateStock();
        throw new RuntimeException();
    }


    @Override
    public void publicFunctionCaller (){
        publicCallee();
    }

    @Override
    @Transactional
    public void publicCallee(){
        goodsStockMapper.updateStock();
        throw new RuntimeException();
    }

    @Autowired
    private GoodsStockService self;

    @Override
    public void aopSelfCaller (){
        self.publicCallee();
    }
}
