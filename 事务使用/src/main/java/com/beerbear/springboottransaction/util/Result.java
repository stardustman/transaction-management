package com.beerbear.springboottransaction.util;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class Result<T> implements Serializable {

    public static int SUCCESS_CODE = 200;
    public static int FAILURE_CODE = -200;

    //继承Serializable接口，实现序列化，并且添加下面这句话还显式地添加序列化
    private static final long serialVersionUID = 1L;

    //业务码，比如成功、失败、权限不足等 code，可自行定义
    @ApiModelProperty("状态码")
    private int resultCode;
    //返回信息，后端在进行业务处理后返回给前端一个提示信息，可自行定义
    @ApiModelProperty("返回备注信息")
    private String message;

    //data这个成员变量的类型为T,T的类型由外部指定
    //数据结果，泛型，可以是列表、单个对象、数字、布尔值等
    @ApiModelProperty("返回数据")
    private T data;

    public Result() {
    }

    public Result(int resultCode) {
        this.resultCode = resultCode;
    }

    public Result(int resultCode, String message) {
        this.resultCode = resultCode;
        this.message = message;
    }

    public int getResultCode() {
        return resultCode;
    }

    /**
     * 直接调用该方法构建一个成功状态的返回实体
     * @return
     */
    public static Result ok(){
        return new Result(SUCCESS_CODE);
    }

    /**
     * 直接调用该方法构建一个失败状态的返回实体
     * @return
     */
    public static Result server_error(){
        return new Result(FAILURE_CODE);
    }

    /**
     * 设置消息
     * @return
     */
    public Result Message(String message){
        this.message = message;
        return this;
    }

    /**
     * 设置返回数据
     * @return
     */
    public Result Data(T data){
        this.data = data;
        return this;
    }
}
